package com.zephaniahnoah.delver;

import com.interrupt.dungeoneer.game.Level;

public abstract class ModEventListener {

	// Called when a level is created
	public void levelGenEvent(Level level) {
	}

	// Called after a level is initialized
	public void levelPostInit(Level level) {
	}
}