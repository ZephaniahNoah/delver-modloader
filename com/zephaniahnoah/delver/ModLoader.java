package com.zephaniahnoah.delver;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.interrupt.dungeoneer.game.ModManager;
import com.interrupt.dungeoneer.scripting.ScriptingApi;
import com.interrupt.utils.Logger;

public class ModLoader implements ScriptingApi {

	public static final List<ModEventListener> listeners = new ArrayList<ModEventListener>();
	final String modFolder = "java";
	public static final URLClassLoader classLoader;

	static {
		classLoader = URLClassLoader.newInstance(new URL[] {}, ClassLoader.getSystemClassLoader());
	}

	@Override
	public void loadScripts(ModManager modManager) {
		listeners.clear();
		Array<String> modClassNames = new Array<String>();

		// Go collect all the new class files
		ArrayMap<FileHandle, FileHandle[]> classFiles = modManager.getFilesForModsWithSuffix(modFolder, ".class");
		for (int i = 0; i < classFiles.size; i++) {
			FileHandle[] mod_classes = classFiles.getValueAt(i);

			for (int ii = 0; ii < mod_classes.length; ii++) {
				FileHandle classFile = mod_classes[ii];

				String classFilePath = classFile.pathWithoutExtension();
				String className = classFilePath.substring(classFilePath.indexOf(modFolder) + modFolder.length() + 1);

				// Add this class name so that we can sandbox it later
				modClassNames.add(className.replace('/', '.').replace('\\', '.'));
			}
		}

		// Finally, let the game know about these new classes
		Array<FileHandle> classFolders = modManager.getFileInAllMods(modFolder);
		for (int i = 0; i < classFolders.size; i++) {
			FileHandle dir = classFolders.get(i);
			addDirectoryToClassLoader(dir);
		}
	}

	private void addDirectoryToClassLoader(FileHandle directory) {
		try {
			URL url = directory.file().toURI().toURL();

			// Use reflection to get the "addURL" method to add this directory to the classpath at runtime
			Method method = URLClassLoader.class.getDeclaredMethod("addURL", new Class[] { URL.class });
			method.setAccessible(true);

			method.invoke(classLoader, new Object[] { url });

			// Load mod event listeners
			File file = Paths.get(url.toURI()).toFile();
			tryLoadFilesIn(file, directory);

		} catch (Exception ex) {
			Logger.logExceptionToFile("Scripting", "Failed to add directory to ClassLoader", ex);
		}
	}

	private void tryLoadFilesIn(File file, FileHandle folder) {
		if (file.isDirectory()) {
			File[] listOfFiles = file.listFiles();
			for (File f : listOfFiles) {
				tryLoadFilesIn(f, folder);
			}
		} else {
			// Remove directory so only the package part is remaining (+ 1 for the / at the beginning)
			String name = file.getAbsolutePath().substring(folder.file().getAbsolutePath().length() + 1, file.getAbsolutePath().length());

			// Remove file extension ".class"
			name = name.substring(0, name.length() - 6);

			// Replace all / and \ with .
			name = name.replace('/', '.');
			name = name.replace('\\', '.');

			try {
				Class<?> clazz = classLoader.loadClass(name);

				if (ModEventListener.class.isAssignableFrom(clazz)) {
					ModLoader.listeners.add((ModEventListener) clazz.newInstance());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}