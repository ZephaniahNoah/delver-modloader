# Delver ModLoader

This is a mod loader for [DelverEngine](https://github.com/Interrupt/delverengine).

It is basically a copy of the [SciptLoader](https://github.com/Interrupt/delverengine/blob/master/Dungeoneer/src/com/interrupt/dungeoneer/scripting/ScriptLoader.java) but without the compiler and without all the security features. It loads mods in pretty much the same way the old scripting_test mods were loaded but doesn't require launch arguments to function. 

The unsandboxed nature of this mod loader means more freedom for modders to do reflection, bytecode injection, and networking. This obviously also means mods with custom code could do malicious things if the developer really wanted it to. If you don't trust a mod then don't use it. I am not responsible for anything that other mods might do to your system.

However, these open source mods were created by me:
* [Bountiful NPCs](https://gitlab.com/ZephaniahNoah/bountiful-npcs)
* [Conducted](https://gitlab.com/ZephaniahNoah/conducted)

Delver ModLoader makes minor changes to four of Delver's base classes.
* `com.interrupt.dungeoneer.game.Level`
* `com.interrupt.dungeoneer.game.ModManager`
* `com.badlogic.gdx.utils.reflect.ClassReflection`
* `com.esotericsoftware.kryo.util.ClassResolver`

# Download
Go to [releases](https://gitlab.com/ZephaniahNoah/delver-modloader/-/releases) to download Delver ModLoader.

# [Creating Mods](https://gitlab.com/ZephaniahNoah/delver-modloader/-/wikis/Creating-Mods)
